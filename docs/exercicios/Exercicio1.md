# Resolução dos exercícios

###1.  Escreva  um  código  em  Python que  peça  dois números  inteiros  e  um  número  real, calculando e mostrando:
i. o  produto do dobro do primeiro com metade do segundo.

ii. a soma do triplo do primeiro com o terceiro.

iii. o terceiro elevado ao cubo.
    
##### Resposta
Código:

    num_1 = int(input("Please, type the first, int, number"))
    num_2 = int(input("Please, type the second, int, number"))
    num_3 = float(input("Please, type the third, real, number"))
    
    i = 2*num_1 + (num_2/2)
    ii = 3*num_1 + num_3
    iii = pow(num_3, 3)
    
    print(f"answer i= {i}, ii={ii}, iii={iii}")


Saída do código acima:
        
    >>> Please, type the first, int, number>? 1
    >>> Please, type the second, int, number>? 2
    >>> Please, type the third, real, number>? 3
    >>> answer i= 3.0, ii=6.0, iii=27.0

###2. Escreva  um  código  em  Python para  a  leitura  de  duas  notas  parciais  de  um  aluno.  
O programa deve calcular a média alcançada por aluno e apresentar:
    
i. a mensagem "Aprovado", se a média alcançada for maiorou igual a sete. 

ii. a mensagem "Reprovado", se a média for menor do que sete.
    
##### Resposta
Código:
        
    value_1 = float(input("Please, enter with the first value"))
    value_2 = float(input("Please, enter with the second value"))
    
    mean = (value_1 + value_2)/2
    
    condition =  "Approved" if(mean>=7) else "Reproved"
    print(condition)

Saída do código acima:

    >>> Please, enter with the first value>? 7
    >>> Please, enter with the second value>? 7
    >>> Approved

###3. Escreva um código em Python que leia três números e mostre o maior e o menor deles.

!!! hint

    Se criado uma lista para armazenar os valores, 
    existe uma função python que retorna o maior 
    valor de um conjunto: A função [max()](https://docs.python.org/3/library/functions.html)

##### Resposta
Código:
        
    numbers = []
    for i in range(0,3):
        value = float(input(f"Enter with the value number {i+1}"))
        numbers.append(value)
    
    print(f"The max value is {max(numbers)}")

Saída do código acima:

    >>> Enter with the value number 1>? 1
    >>> Enter with the value number 2>? 2
    >>> Enter with the value number 3>? 3
    >>> The max value is 3.0

###4. Escreva um código em Python que leia um númeroe em seguida pergunte ao usuário qual informação ele deseja saber, ou seja, se o número é: 

a. par ou ímpar. 

b. positivo ou negativo. 

c. inteiro ou decimal.

O programa deve ser acompanhado de uma frase que informe o resultado da informação escolhida.

#### Resposta
Código:
    
    def choices(num):
    
        value = str(
            input(
                "Choose your option-> a: know "
                 "if is pair or odd, b: positive or negative -> c: int or decimal: "
            )
        )
    
        if value.lower() == 'a':
            if num % 2 == 0:
                return print(f"The number {num} is pair")
            else:
                return print(f"The number {num} is odd")
    
        elif value.lower() == 'b':
            if num > 0:
                return print(f"The number {num} is positive ")
            else:
                return print(f"The number {num} is negative")
    
        else:
            if isinstance(num, int):
                return print(f"The number {num} is Int")
            else:
                return print(f"The number {num} is {type(num)}")

Teste:

    if __name__ == '__main__':
        choices(3)

Saída do código acima:
    
    >>> Choose your option-> a: know if is pair or odd, b: positive or negative -> c: int or decimal: a
    >>> The number 3 is odd

    >>> Choose your option-> a: know if is pair or odd, b: positive or negative -> c: int or decimal: b
    >>> The number 3 is positive

    >>> Choose your option-> a: know if is pair or odd, b: positive or negative -> c: int or decimal: c
    >>> The number 3 is Int

###5. Escreva  um  código  em  Python que  receba  dois  números  inteiros  e  gere  os  números inteiros que estão no intervalo compreendido por eles.

!!! hint

    A Função range() gera os números entre um dado intervalo.

##### Resposta
Código:
    
    value_1 = int(input(f"Enter with the first value"))        
    value_2 = int(input(f"Enter with the second value"))        
    
    for i in range(value_1, value_2):
        print(i)


Saída do código acima:

    >>> Enter with the first value>? 1
    >>> Enter with the second value>? 3
    >>> 1
    >>> 2


###6. Escreva um código em Python,com uma função que necessite de três argumentos, e que forneça a soma desses três argumentos.

!!! hint 

    Para funções de estrutura menos complexas, com cálculos
    menores, pode-se utilizar funções anônimas ou lambda functions.

####Resposta
Código:
    
    sum = lambda num1, num2, num3: print(num1+num2+num3)
    sum(1,2,3)

Saída do código acima:
    
    >>> 6

###7. Escreva uma função em Python que informe a quantidade de dígitos de um determinado número inteiro informado.

!!! hint

    Existe uma função pronta para contar a quantidades,
    de caracteres em uma string em python: A função len().
    Porém, tem-se que converter uma variável inteira em string.

####Resposta
Código:

    length = lambda num1: print(len(str(num1)))
    length(123456789)

Saída do código acima:

    >>> 9

###8. Escreva uma função em Python que retorne o reverso de um número inteiro informado. 

!!! example

    Por exemplo: 127 -> 721.

####Resposta
Código:

    num = str(input("Enter with a number")) #input the number in a string format
    invert_number = lambda num1: print((num1)[::-1]) #invert the string function
    invert_number(num) # call the function  

Saída do código acima:

    >>> Enter with a number>? 12345
    >>> 54321

###9. Escreva em Python a classe Funcionário. Um funcionário tem um nome (um string) e um salário (um double). Escreva um construtor com dois parâmetros (nome e salário) e métodos para devolver nome e salário. Implemente um pequeno programa que teste sua classe.

!!! hint

    Uma das boas praticas em classes python são as Docstrings.
    Além disso pode-se, nos métodos, atribuir quais são os tipos das variáveis
    que `espera-se` receber. 

!!! note

    `OBS`: Python não irá impedir de que seja enviado um tipo de dado diferente naquele que 
    foi documentado no método, apenas serve para instrução ao desenvolvedor que for utilizar.

####Resposta
Código:
        
    class Funcionario(object):
        """
        Class to represents Funcionário's model
        """
        def __init__(self, nome: str, salario: float):
            self.nome = nome
            self.salario = salario
    
        @property
        def nome(self):
            return self.__nome.title()
    
        @nome.setter
        def nome(self, nome):
            self.__nome = nome.title()
    
        @property
        def salario(self):
            return self.__salario
    
        @salario.setter
        def salario(self, salario):
            self.__salario = salario


    if __name__ == '__main__':
        funcionario1 = Funcionario("vitor", 1000.00)
        print(funcionario1.nome)
        print(funcionario1.salario)

Saída do código acima:

    >>> Vitor
    >>> 1000.0
    
!!! note

    `Getters` and `Setters` são propriedades de uma classe para ler ou 
    atribuir um valor a um atributo da classe.
    Eles `sobrescrevem` o método build in da classe que retorna o valor do atributo
    ou que insere valor no atributo.
    É util quando o codigo começa a ficar extenso e necessita de mudanças na classe,
    que irá afetar todos os trechos de códigos onde são utilizadas.

###10. Escreva em Pytho numa classe para implementar uma conta corrente. A classe deve possuir os seguintes atributos: número da conta, nome do correntista e saldo. Os métodos são  os  seguintes:  alterarNome,  depósito  e  saque.No construtor,  saldo  é  opcional,  com valor default zero e os demais atributos são obrigatórios.

####Resposta
Código:
    
    class Funcionario(object):
        """
        Class to represents Funcionário's model
        """
        def __init__(self, nome, salario):
            self.nome = nome
            self.salario = salario
    
        @property
        def nome(self):
            return self.__nome.title()
    
        @nome.setter
        def nome(self, nome):
            self.__nome = nome.title()
    
        @property
        def salario(self):
            return self.salario
    
        @salario.setter
        def salario(self, salario):
            self.__salario = salario


    class ContaCorrente(Funcionario):
        """
        Conta Corrente de um funcionario
        """
    
        def __init__(self, nome, salario, saldo=None):
            self.nome = nome
            self.salario = salario
            self.numero_da_conta = uuid.uuid4()
            if saldo:
                self.saldo = saldo
    
        def saque(self, valor):
            if valor> self.saldo:
                return "Saldo insuficiente"
            else:
                self.saldo = self.saldo - valor
                return "Saque feito com sucesso"
        
        def deposito(self, valor):
            self.saldo += valor
            return "Deposito feito com sucesso"
    
Teste:

    if __name__ == '__main__':
        contaCorrente = ContaCorrente("vitor", 1000.00, 2000)
       
        contaCorrente.nome = 'Joao' # Altera o nome
        
        print(contaCorrente.saque(200000)) # saque
        print(contaCorrente.deposito(200000)) # deposito
        print(contaCorrente.nome)
        print(contaCorrente.salario)
        

Saída do código acima:
    
    >>> Saldo insuficiente
    >>> Deposito feito com sucesso
    >>> Joao
    >>> 1000.0
    

!!! hint

    Nesse exercício foi utlizado herança de classes. A classe ContaCorrente herda
    de funcionário, e é do tipo funcionário. Por herdar dessa classe, apresenta 
    os mesmos atributos e métodos da classe pai.