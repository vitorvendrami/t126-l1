# T126-L1

Resolução de Listas de exercícios.

## Tecnologias

* `mkdocs ` - Lib to document a project.
* `python` - Programing Language.
* `Git Lab CI/CD` - Continues Delivery/Integration to deploy.
* `Git` - Code versioning.

## Resoluções

1. Veja a resolução da [Lista 1](exercicios/list_index_1.md)

